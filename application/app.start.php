<?php

helper(['eyeem', 'session']);

start_session();

$photo_query_parameters = ['limit' => 100];

function any_printers()
{
  return json_decode(file_get_contents(ANY_PRINTER_API_URL . '/printers'), true);
}

function any_print($printer_id, $photo_url)
{
  $url  = ANY_PRINTER_API_URL . '/printers/' . $printer_id . '/jobs';
  $url .= '?client_id=' . ANY_PRINTER_CLIENT_ID;
  $url .= '&client_secret=' . ANY_PRINTER_CLIENT_SECRET;
  $url .= '&photo_url=' . $photo_url;
  $url .= '&forceMethod=POST';
  $result = file_get_contents($url);
  error_log($result);
  return true;
}

# EyeEm Authentication
if (url_is('/auth/signin')) {
  $redirect_url = eyeem()->getLoginUrl(absolute_url('/auth/callback'));
  return redirect($redirect_url);
}
elseif (url_is('/auth/signout')) {
  signout();
  return redirect('/');
}
elseif (url_is('/auth/callback')) {
  if (has_param('code')) {
    $token = eyeem()->getToken(get_param('code'));
    signin($token['access_token']);
  }
  return redirect('/friends');
}

# EyeEm Photo Pages
elseif (url_is('/') || url_is('/popular')) {
  return render('index', ['photos' => eyeem()->getPopularPhotos($photo_query_parameters)]);
}
elseif (url_is('/friends')) {
  if (is_authenticated()) {
    return render('index', ['photos' => authenticated_user()->getFriendsPhotos($photo_query_parameters)]);
  }
  else {
    return redirect('/');
  }
}
elseif (url_is('/likes')) {
  if (is_authenticated()) {
    return render('index', ['photos' => authenticated_user()->getLikedPhotos($photo_query_parameters)]);
  }
  else {
    return redirect('/');
  }
}
elseif (url_is('/my')) {
  if (is_authenticated()) {
    return render('index', ['photos' => authenticated_user()->getPhotos($photo_query_parameters)]);
  }
  else {
    return redirect('/');
  }
}

# Print Page
elseif ($matches = url_match('/p/*/print')) {
  $photo = eyeem()->getPhoto($matches[1]);
  if (is_post()) {
    check_parameters(['printer_id']);
    $orientation = $photo->getWidth() > $photo->getHeight() ? 'w' : 'h';
    any_print(get_param('printer_id'), $photo->getThumbUrl($orientation, 2048));
    return render('confirm', ['photo' => $photo]);
  }
  else {
    return render('print', ['photo' => $photo]);
  }
}
