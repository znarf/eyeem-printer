<?php

$eyeem = new Eyeem();
$eyeem->setClientId(EYEEM_CLIENT_ID);
$eyeem->setClientSecret(EYEEM_CLIENT_SECRET);

replaceable('eyeem', function() use($eyeem) { return $eyeem; });

return $eyeem;
