<?php

function start_session()
{
  if (!session_id()) {
    session_start();
  }
  if (isset($_SESSION['access_token'])) {
    helper('eyeem')->setAccessToken($_SESSION['access_token']);
  }
}

function is_authenticated()
{
  return isset($_SESSION['access_token']);
}

function signin($access_token)
{
  $_SESSION['access_token'] = $access_token;
  return true;
}

function signout()
{
  $_SESSION['access_token'] = null;
  $_SESSION['raw_authenticated_user'] = null;
  return true;
}

function authenticated_user()
{
  if (isset($_SESSION['raw_authenticated_user'])) {
    return helper('eyeem')->getAuthUser($_SESSION['raw_authenticated_user']);
  } else {
    $authenticated_user = helper('eyeem')->getAuthUser();
    $_SESSION['raw_authenticated_user'] = $authenticated_user->getRawArray();
    return $authenticated_user;
  }
}
