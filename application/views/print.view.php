<?php foreach (any_printers() as $printer) : ?>

<form method="post" action="">
  <input type="hidden" name="printer_id" value="<?= $printer['id'] ?>">
  <input type="submit" value="Print on <?= $printer['name'] ?>">
</form>

<?php endforeach ?>

<?php if ($photo->getWidth() > $photo->getHeight()) : ?>

<div class="photo big landscape">
<img src="<?= $photo->getThumbUrl('w', '512') ?>">
</div>

<?php else : ?>

<div class="photo big portrait">
<img src="<?= $photo->getThumbUrl('h', '512') ?>">
</div>

<?php endif ?>
