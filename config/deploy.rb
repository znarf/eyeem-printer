# sudo gem install capistrano capistrano-ext

require 'capistrano/ext/multistage'

set :stages, %w(production staging)

set :application, "EyeEm Printer"

set :scm, :git

set :keep_releases, 3

set :git_enable_submodules, 1

set :shared_children,   %w(tmp)

set :shared_files,      %w(config/config.php)

namespace :deploy do
  desc "Reload nginx & php5-fpm"
  task :restart do
    run "sudo service nginx reload"
    run "sudo service php5-fpm reload"
  end
  desc "Symlink static directories and static files that need to remain between deployments."
  task :share_childs do
    if shared_children
      shared_children.each do |link|
        run "#{try_sudo} mkdir -p #{shared_path}/#{link}"
        run "#{try_sudo} sh -c 'if [ -d #{release_path}/#{link} ] ; then rm -rf #{release_path}/#{link}; fi'"
        run "#{try_sudo} ln -nfs #{shared_path}/#{link} #{release_path}/#{link}"
      end
    end
    if shared_files
      shared_files.each do |link|
        link_dir = File.dirname("#{shared_path}/#{link}")
        run "#{try_sudo} mkdir -p #{link_dir}"
        run "#{try_sudo} touch #{shared_path}/#{link}"
        run "#{try_sudo} ln -nfs #{shared_path}/#{link} #{release_path}/#{link}"
      end
    end
  end
  desc "Customize the finalize_update task"
  task :finalize_update, :except => { :no_release => true } do
    run "#{try_sudo} chmod -R g+w #{latest_release}" if fetch(:group_writable, true)
    share_childs
  end
end

after "deploy", "deploy:cleanup"
