EyeEm Printer
-----

This app is made of Amateur, Bootstrap, jQuery and the EyeEm PHP wrapper.

Setup
-----

1. Clone this repository.

```
git clone git@bitbucket.org:znarf/eyeem-printer.git
cd eyeem-printer
git submodule update --init
```

2. Register a new Application at http://www.eyeem.com/developers

3. Copy config-sample.php to config/config.php with your Client Id and Client Secret.

4. From PHP 5.4, you can simply run:

```
php -S localhost:8008 -t web
```