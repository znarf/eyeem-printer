<?php

define('root_dir', dirname(__DIR__));

define('app_dir', root_dir . '/application');

include root_dir . '/config/config.php';

include root_dir . '/vendor/eyeem/php-wrapper/autoload.php';

require root_dir . '/vendor/amateur/amateur.dsl.php';

require_once amateur_dir . '/core/replaceable.functions.php';

start( app_dir );
